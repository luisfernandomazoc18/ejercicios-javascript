# JAVASCRIPT

## Tarea 6 
Trabajo en clase realizar los siguientes 20 Ejercicios en parejas utilizando el lenguaje JavaScript

## Ejercicio 1
Escribí un programa que solicite al usuario que ingrese su nombre. El nombre se debe almacenar en una variable llamada nombre. A continuación se debe mostrar en pantalla el texto “Ahora estás en la matrix, [usuario]”, donde “[usuario]” se reemplazará por el nombre que el usuario haya ingresado.

### Ejemplo de ejecución
```bash
Tu nombre: Patricia
Ahora estás en la matrix, Patricia
```

### Código
```javascript
var nombre = prompt("dime tu nombre");
console.log("Ahora estas en la matriz "+ [nombre]);
```

## Ejercicio 2
Escribí un programa que solicite al usuario ingresar un número con decimales y almacénalo en una variable. A continuación, el programa debe solicitar al usuario que ingrese un número entero y guardarlo en otra variable. En una tercera variable se deberá guardar el resultado de la suma de los dos números ingresados por el usuario. Por último, se debe mostrar en pantalla el texto “El resultado de la suma es [suma]”, donde “[suma]” se reemplazará por el resultado de la operación.

### Ejemplo de ejecución
``` bash
Primer número: 14.2
Segundo número: 19
El resultado de la suma es 33.2
```

### Código
```javascript
var decimal = prompt("Ingrese un número decimal");
var entero = prompt("Ingrese un número Entero");
var suma = parseFloat(decimal) + parseInt(entero);
console.log("El resultado de la suma es "+ [suma]);
```

## Ejercicio 3
Escribí un programa que solicite al usuario dos números y los almacene en dos variables. En otra variable, almacená el resultado de la suma de esos dos números y luego mostrá ese resultado en pantalla.
A continuación, el programa debe solicitar al usuario que ingrese un tercer número, el cual se debe almacenar en una nueva variable. Por último, mostrá en pantalla el resultado de la multiplicación de este nuevo número por el resultado de la suma anterior.

### Ejemplo de ejecución
```bash
Ingresá un número: 1
Ingresá otro número: 2
Suman: 3
Ingresá un nuevo número: 3
Multiplicación de la suma por el último número: 9
```

### Código
```javascript
var A = prompt("Ingrese un número ");
var B = prompt("Ingrese otro número ");
var suma = parseFloat(A) + parseInt(B);
console.log("El resultado de la suma es "+ [suma]);
var numeroNuevo = prompt("Ingrese un número ");
var resultado = numeroNuevo*suma;
console.log("La multiplicación de la suma por el último número ingresado es: "+ resultado);
```

## Ejercicio 4
Escribí un programa que solicite al usuario ingresar la cantidad de kilómetros recorridos por una motocicleta y la cantidad de litros de combustible que consumió durante ese recorrido. Mostrar el consumo de combustible por kilómetro.

### Ejemplo de ejecución
```bash
Kilómetros recorridos: 260
Litros de combustible gastados: 12.5
El consumo por kilómetro es de 20.8
```

### Código
```javascript
var kilometros = prompt("Ingrese cantidad de kilometraje ");
var combustible = prompt("Ingrese cantidad de combustible consumido ");
var consumoPorKilometro = kilometros/combustible;
console.log("El consumo por kilometro es de "+ consumoPorKilometro);
```

## Ejercicio 5
Escribí un programa que solicite al usuario el ingreso de una temperatura en escala Fahrenheit (debe permitir decimales) y le muestre el equivalente en grados Celsius. La fórmula de conversión que se usa para este cálculo es: _Celsius = (5/9) * (Fahrenheit-32)_

### Ejemplo de ejecución
```bash
Ingresá una temperatura expresada en Fahrenheit: 75
23.88888888888889
```

### Código
```javascript
var temperatura = prompt("Ingrese una temperatura expresada en Fahrenheit");
var gradosC = ((5/9)*(temperatura-32));
console.log("La temperatura en grados Celsius es de "+ gradosC);
```

## Ejercicio 6
Escribí un programa que solicite al usuario ingresar tres números para luego mostrarle el promedio de los tres.

### Ejemplo de ejecución
```bash
Primer número: 8.5
Segundo número: 10
Tercer número: 5.5
El promedio de los tres es 8.0
```

### Código
```javascript
var numeroPromedio1 = prompt("Ingrese primer numero ");
var numeroPromedio2 = prompt("Ingrese segundo numero ");
var numeroPromedio3 = prompt("Ingrese tercer numero ");
var sumaPromedio = (parseInt(numeroPromedio1) + parseInt(numeroPromedio2) + parseInt(numeroPromedio3));
console.log("El promedio de los 3 números es: "+ sumaPromedio/3);
```

## Ejercicio 7
Escribí un programa que solicite al usuario un número y le reste el 15%, almacenando todo en una única variable. A continuación, mostrar el resultado final en pantalla.

### Ejemplo de ejecución
```bash
Ingresá un número: 260
Descontando el 15% queda: 221.0
```

### Código
```javascript
var numDescuento = prompt("Ingrese un número");
var resultadoDescuento =  (parseInt(numDescuento)-(parseInt(numDescuento)*0.15));
console.log("El numero aplicado el descuento del 15% es: "+ resultadoDescuento );
```

## Ejercicio 8
Escribí un programa que solicite al usuario el ingreso de dos palabras, las cuales se guardarán en dos variables distintas. A continuación, almacená en una variable la concatenación de la primera palabra, más un espacio, más la segunda palabra. Mostrá este resultado en pantalla.

### Ejemplo de ejecución
```bash
Primera palabra: derribando
Segunda palabra: muros
derribando muros
```

### Código
```javascript
var palabra1 = prompt("Ingrese una palabra");
var palabra2 = prompt("Ingrese otra palabra");
console.log(palabra1 + " " + palabra2);
```

## Ejercicio 9
Escribí un programa que solicite al usuario el ingreso de un texto y almacene ese texto en una variable. A continuación, mostrar en pantalla la primera letra del texto ingresado. Luego, solicitar al usuario que ingrese un número positivo menor a la cantidad de caracteres que tiene el texto que ingresó (por ejemplo, si escribió la palabra “HOLA”, tendrá que ser un número entre 0 y 4) y almacenar este número en una variable llamada índice.
Mostrar en pantalla el carácter del texto ubicado en la posición dada por índice.

### Ejemplo de ejecución
```bash
Ingresá un texto: En un lugar de La Mancha, de cuyo nombre no quiero acordarme…
El carácter en primer lugar es: E
Ingresá un número positivo menor a 63: 7
El carácter en esa posición es: u
```

### Código
```javascript
var texto = prompt("Ingresá un texto");
var primerCaracter = texto.charAt(0);
console.log("El carácter en el primer lugar es: " + primerCaracter);
var contarCaracteres = texto.length;
var numCaracter = prompt("Ingresá un número positivo menor a " + contarCaracteres);
console.log("El carácter en esa posición es " + texto.charAt(numCaracter));
```

## Ejercicio 10
Escribí un programa que solicite al usuario que ingrese cuántos shows musicales ha visto en el último año y almacene ese número en una variable. A continuación mostrar en pantalla un valor de verdad (True o False) que indique si el usuario ha visto más de 3 shows.

### Ejemplo de ejecución
```bash
Shows vistos en el último año: 3
False
```

### Código
```javascript
var numShows = prompt("Shows vistos en el último año");
if(numShows<=3){
    console.log("False");
}else{
    console.log("True");
}
```

## Ejercicio 11
Escribí un programa que le solicite al usuario ingresar una fecha formada por 8 números, donde los primeros dos representan el día, los siguientes dos el mes y los últimos cuatro el año (DDMMAAAA). Este dato debe guardarse en una variable con tipo int (número entero). Finalmente, mostrar al usuario la fecha con el formato DD / MM / AAAA.

### Ejemplo de ejecución
```bash
Fecha en formato DDMMAAAA: 16112017
16 / 11 / 2017
```

### Código
```javascript
var fecha =  prompt("Ingresá la fecha en formato DDMMAAAA:");
var dia = Math.trunc(fecha/1000000);
var mes = Math.trunc(fecha/10000)%100;
var año = fecha%10000;
console.log(dia + " / " + mes + " / " + año);
```

## Ejercicio 12
Escribí un programa para solicitar al usuario el ingreso de un número entero y que luego imprima un valor de verdad dependiendo de si el número es par o no. Recordar que un número es par si el resto, al dividirlo por 2, es 0.

### Ejemplo de ejecución
```bash
Número entero: 7254
True
```

### Código
```javascript
var numEntero = prompt("Ingresá un número entero");
if(numEntero%2 == 0){
    console.log("True");
}else{
    console.log("False");
}
```

## Ejercicio 13
Escribí un programa que le solicite al usuario su edad y la guarde en una variable. Que luego solicite la cantidad de artículos comprados en una tienda y la guarde en otra variable. Finalmente, mostrar en pantalla un valor de verdad (True o False) que indique si el usuario es mayor de 18 años de edad y además compró más de 1 artículo.

### Ejemplo de ejecución
```bash
Tu edad: 32
Artículos comprados: 1
False
```

### Código
```javascript
var edad = prompt("Ingresá la edad");
var compras = prompt("Ingresá la cantidad de compras");
if(edad>=18 && compras>1){
    console.log("True");
}else{
    console.log("False");
}

## Ejercicio 14
Escribí un programa que, dada una cadena de texto por el usuario, imprima True si la cantidad de caracteres en la cadena es un número impar, o False si no lo es.

### Ejemplo de ejecución
```bash
Ingresá una frase: Era el mejor de los tiempos, era el peor de los tiempos.
True
```

### Código
```javascript
var texto = prompt("Ingresá un texto");
var contarCaracteres = texto.length;
if(contarCaracteres%2){
    console.log("False");
}else{
    console.log("True");
}
```

## Ejercicio 15
Escribí un programa que le pida al usuario ingresar dos palabras y las guarde en dos variables, y que luego imprima True si la primera palabra es menor que la segunda o False si no lo es.

### Ejemplo de ejecución
```bash
Una palabra: complejidad
Otra palabra: algoritmo
False
```

### Código
```javascript
var palabra1 = prompt("Ingresá una palabra");
var palabra2 = prompt("Ingresá una palabra");
var countchar1 = palabra1.length;
var countchar2 = palabra2.length;
if(countchar1<countchar2){
    console.log("True");
}else{
    console.log("False");
}
```

## Ejercicio 16
Escribí un programa para pedir al usuario su nombre y luego el nombre de otra persona, almacenando cada nombre en una variable. Luego mostrar en pantalla un valor de verdad que indique si: los nombres de ambas personas comienzan con la misma letra ó si terminan con la misma letra. Por ejemplo, si los nombres ingresados son María y Marcos, se mostrará True, ya que ambos comienzan con la misma letra. Si los nombres son Ricardo y Gonzalo se mostrará True, ya que ambos terminan con la misma letra. Si los nombres son Florencia y Lautaro se mostrará False, ya que no coinciden ni la primera ni la última letra.

### Ejemplo de ejecución
```bash
Tu nombre: Alfredo
Otro nombre: Eduardo
True
```

### Código
```javascript
var nombre1 = prompt("Ingresá tu nombre");
var nombre2 = prompt("Ingresá otro nombre");
var char1nom1 = nombre1.charAt(0);
var char1nom2 = nombre2.charAt(0);
var charfnom1 = nombre1.charAt(nombre1.length - 1);
var charfnom2 = nombre2.charAt(nombre2.length - 1);
if(char1nom1==char1nom2 || charfnom1==charfnom2){
    console.log("True");
}else{
    console.log("False");
}
```

## Ejercicio 17
Escribí un programa que, dado un número entero, muestre su valor absoluto. Recordá que, para los números positivos su valor absoluto es igual al número (el valor absoluto de 52 es 52), mientras que, para los negativos, su valor absoluto es el número multiplicado por -1 (el valor absoluto de -52 es 52).

### Ejemplo de ejecución
```bash
Número: -12
Valor absoluto: 12
```

### Código
```javascript
var numero = prompt("Ingresa un número");
console.log(Math.abs(numero));
```

## Ejercicio 18
Escribí un programa que solicite al usuario el ingreso de dos números diferentes y muestre en pantalla al mayor de los dos.

### Ejemplo de ejecución
```bash
Un número: 592
Otro número distinto: 1726
1726 es mayor
```

### Código
var numero1 = prompt("Ingresá un número");
var numero2 = prompt("Ingresá otro número");
if(parseInt(numero1)>parseInt(numero2)){
    console.log(numero1 + " es mayor");
}else{
    console.log(numero2 + " es mayor");
}

## Ejercicio 19
Escribí un programa que solicite al usuario una letra y, si es una vocal, muestre el mensaje “Es vocal”. Verificar si el usuario ingresó un string de más de un carácter y, en ese caso, informarle que no se puede procesar el dato.

### Ejemplo de ejecución
```bash
Letra: o
Es vocal
```

### Código
```javascript
var letra = prompt("Ingresá una letra");
if(letra.length>1){
    console.log("No se puede procesar el dato");
}else{
    if(letra=="a"||letra=="e"||letra=="i"||letra=="o"||letra=="u"){
        console.log(letra + " es vocal");
    }
}
```

## Ejercicio 20
Escribí un programa para solicitar al usuario tres números y mostrar en pantalla al menor de los tres.

### Ejemplo de ejecución
```bash
Primer número: 20
Segundo número: 30
Tercer número: 10
Menor: 10
```

### Código
```javascript
var numero1 = prompt("Ingresá un número");
var numero2 = prompt("Ingresá otro número");
var numero3 = prompt("Ingresá otro número");
if(parseInt(numero1)<parseInt(numero2) && parseInt(numero1)<parseInt(numero3)){
    console.log("Menor: " + numero1);
}else if(parseInt(numero2)<parseInt(numero1) && parseInt(numero2)<parseInt(numero3)){
    console.log("Menor: " + numero2);
}else{
    console.log("Menor: " + numero3);
}
```
